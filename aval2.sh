#!/bin/bash

# Função para exibir o menu
exibir_menu() {
    echo "Menu:"
    echo "1. Escolher um arquivo .zip existente ou criar um novo"
    echo "2. Listar o conteúdo do arquivo .zip"
    echo "3. Pré-visualizar algum arquivo que está compactado neste zip"
    echo "4. Adicionar arquivos ao zip"
    echo "5. Remover arquivos do zip"
    echo "6. Extrair todo o conteúdo do zip"
    echo "7. Extrair arquivos específicos do zip"
    echo "8. Sair"
}

# Função para escolher ou criar um arquivo .zip
escolher_zip() {
    read -p "Digite o nome do arquivo .zip (ou deixe em branco para criar um novo): " zip_file
    touch "$zip_file"
}

# Função para listar o conteúdo do arquivo .zip
listar_conteudo() {
    unzip -l "$zip_file"
}

# Função para pré-visualizar um arquivo dentro do .zip
visualizar_arquivo() {
    read -p "Digite o nome do arquivo para pré-visualização: " arquivo
    unzip -c "$zip_file" "$arquivo"
}

# Função para adicionar arquivos ao .zip
adicionar_arquivos() {
    read -p "Digite o(s) nome(s) do(s) arquivo(s) para adicionar (separados por espaço): " arquivos
    zip -u "$zip_file" $arquivos
}

# Função para remover arquivos do .zip
remover_arquivos() {
    read -p "Digite o(s) nome(s) do(s) arquivo(s) para remover (separados por espaço): " arquivos
    zip -d "$zip_file" $arquivos
}

# Função para extrair todo o conteúdo do .zip
extrair_tudo() {
    unzip "$zip_file"
}

# Função para extrair arquivos específicos do .zip
extrair_arquivos_especificos() {
    read -p "Digite o(s) nome(s) do(s) arquivo(s) para extrair (separados por espaço): " arquivos
    unzip "$zip_file" $arquivos
}

# Loop principal
while true; do
    exibir_menu
    read -p "Escolha uma opção (1-8): " opcao

    case $opcao in
        1) escolher_zip ;;
        2) listar_conteudo ;;
        3) visualizar_arquivo ;;
        4) adicionar_arquivos ;;
        5) remover_arquivos ;;
        6) extrair_tudo ;;
        7) extrair_arquivos_especificos ;;
        8) echo "Saindo. Até mais!"; exit ;;
        *) echo "Opção inválida. Tente novamente." ;;
    esac
done
